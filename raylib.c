#include <stdio.h>
#include <raylib.h>

int main(void) {

  //Pop up a window
  InitWindow(800, 600, "text");

  //init your audio device
  InitAudioDevice();

  //load the music file
  Music music =  LoadMusicStream("musicf.mp3");
  PlayMusicStream(music);

  Image image = LoadImage("play.png");
  SetWindowIcon(image);
  UnloadImage(image);
  
  while (!WindowShouldClose()) {
    
    UpdateMusicStream(music);
    
    if (IsKeyPressed(KEY_SPACE))
      {
	if (IsMusicStreamPlaying(music))
	  {
	    PauseMusicStream(music);
	  } else {
	  ResumeMusicStream(music);
	}
      }

    BeginDrawing();
    
    ClearBackground(RAYWHITE);

    // Get and display music time played 
    int timePlayed = GetMusicTimePlayed(music);
    Vector2 textSize = MeasureTextEx(GetFontDefault(), TextFormat("%02d:%02d", timePlayed / 60, timePlayed % 60), 60, 0);
    int posX = (GetScreenWidth() - textSize.x) / 2;
    int posY = (GetScreenHeight() - textSize.y) / 2;
    
    DrawText(TextFormat("%02d:%02d", timePlayed / 60, timePlayed % 60), posX, posY, 50, BLACK);

    EndDrawing();
  }
  
  UnloadMusicStream(music);
  CloseAudioDevice();

  return 0;
}
